# Redirect Proxy

An http(s) proxy that intercepts requests and

1. intercepts `http://foo.com/assets/bar.html` and serves the static asset file `./html/bar.html`
2. (optionally) redirects JS library requests to `localhost:4200` or some other specified URL,
based on a provided list of library host names

The only requirement is Docker, although helper scripts for running the proxy use bash.

## Use Case

Let's say you have a JS library that is minified and distributed by a production
server or CDN that is designed to work with some kind of on-page integration.  
For example, the library might expose a JavaScript API that the website developer can interact with on the page.
Additionally the JS library may impose restrictions on the domain serving the JS, in order to 
prevent usage on unauthorized domains. If you don't have access to a website's source (i.e. a
customer) but want to test sample integrations with the library (possibly with different 
browsers), then you can use this tool.

## Setup

### Obtaining the SSL CA

After running the proxy for the first time a cert should be generated at ~/.mitmproxy/mitmproxy-ca-cert.pem
Add this to your browser.  Use the .p12 version for IE.

You can also download the certificate from a browser connected to the proxy at the url
`http://example.com/assets/ca-cert.p12` or `http://example.com/assets/ca-cert.pem`

### Installing on Chrome and Enabling Proxy

* Follow the steps [here](http://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate) to install the certificate as an Authority
* You may want to install a browser extension to easily toggle proxy settings to `http://localhost:8080`

## Running

Run `$ ./proxy.sh` script to start the proxy with default options. You can also include the following flags:

```
-h:         display usage details

-b:         force a rebuild of the docker image. Useful for developing on redirect-proxy
            Disabled by default
						
-r:         redirect JS library requests 
            disabled by default unless -u flag present

-u:         redirect URL for JS requests, if applicable
            defaults to http://localhost:4200
            
-j          comma separated list of JS library host names
            requests matching these hosts will be redirected to the specified host            
```

For example, for a JS library hosted at `js.mydomain.com/library.min.js`
and distributed by a CDN at `my-cdn.mydomain.com`, instruct the proxy to redirect those
requests to a local server running on port 5000:
```bash
$ ./proxy.sh -u localhost:5000 -j js.mydomain.com,my-cdn.mydomain.com
```

Once the proxy is running and the browser is connected to it, you can verify it's
working by visiting http://example.com/assets/hello-world.html.

## Development

### Rebuilding the Image

When running redirect-proxy the sources are copied into in the image at build time. The proxy.sh script will
not rebuild with Docker if it finds a tagged image already present, so in order to propagate changes
to the source files, force a rebuild with the `-b` flag: 

```bash
$ ./proxy.sh -b
```

### Running Tests

If developing on redirect-proxy, run the unit tests with an optional `--watch` flag to rerun tests on file changes:

```bash
$ ./run-tests [--watch] [--rebuild]
```

If updating dependencies in `requirements.txt` you will also need to provide the `--rebuild` option.

## Portability Note

In theory the proxy and tests can run on any machine with Docker, however the scripts outlined above for starting
the proxy assume Linux support for bash.
In order to run on a system not supporting bash, use Docker build and run commands directly, passing
options as environment variables to the container i.e.  
```bash
    docker build -t redirect-proxy .
    docker run -it --rm \
      --network host \
      --env REDIRECT_JS=true \
      --env REDIRECT_URL=localhost:5000 \
      --env JS_LIB_HOSTS=cdn.myhost.com \
    	-v <path to pwd>/assets/:/assets \
    	redirect-proxy
```
