#!/bin/bash

# parse arguments
while [[ $# -gt 0 ]]; do
  key=$1
  case $key in
    --rebuild|-b)
      REBUILD_IMAGE=true
      ;;
    --watch|-w)
      WATCH_TESTS=true
      ;;
    --help|-h)
      cat << HERE
Usage: "$0" [-b | --rebuild] [-w | --watch]
HERE
      exit 0
      ;;
  esac
  shift
done

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
THIS_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# only build image if not already present or rebuild is forced
if [[ "$REBUILD_IMAGE" == "true" ]] || [[ "$(docker images -q proxy-redirect-test:latest 2> /dev/null)" == "" ]]; then
  docker build -f "${THIS_DIRECTORY}"/Dockerfile.test -t proxy-redirect-test "${THIS_DIRECTORY}"
fi

function block_for_change {
  inotifywait \
    --event modify,move,create,delete \
    ${THIS_DIRECTORY}
}

function run_tests {
  docker run -it --rm \
	  -v "${THIS_DIRECTORY}"/proxy.py:/proxy-redirect/proxy.py \
	  --env REDIRECT_JS=true \
	  --env REDIRECT_URL=http://localhost:4200 \
	  --env JS_LIB_HOSTS=js-cdn.example.com,js.example.com \
	  -v "${THIS_DIRECTORY}"/proxy_test.py:/proxy-redirect/proxy_test.py \
		proxy-redirect-test
}

run_tests
EXIT_CODE=$(echo $?)

if [[ "$WATCH_TESTS" == "true" ]]; then
	while block_for_change; do
	  run_tests
	done
else
	exit $(echo ${EXIT_CODE})
fi
