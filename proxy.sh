#!/bin/bash

function usage() {
    cat <<EOF
usage: proxy.sh [-h | [-r [-u redirect_url]]]
-h:         print this message before exiting
-b:         force a rebuild of the docker image. Useful for developing on the proxy
-r:         redirect JS library requests
            * defaults to false
-u:         redirect URL for JS requests
            * defaults to http://localhost:4200
EOF
}

# parse args to pass to container as env variables
while getopts "hu:br" OPTION
do
  case ${OPTION} in
    h)
      usage
      exit 1
      ;;
    r)
      SET_REDIRECT_FLAG="--env REDIRECT_JS=true"
      ;;
    u)
      SET_REDIRECT_URL="--env REDIRECT_URL=${OPTARG}"
      ;;
    j)
	    SET_JS_HOSTS="--env JS_LIB_HOSTS=${OPTARG}"
	    ;;
    b)
	    REBUILD_IMAGE="true"
	    ;;
  esac
done

# only build image if not already present or rebuild is forced
if [[ "$REBUILD_IMAGE" == "true" ]] || [[ "$(docker images -q redirect-proxy:latest 2> /dev/null)" == "" ]]; then
  docker build -t redirect-proxy .
fi

docker run -it --rm \
  --network host \
  $(echo ${SET_REDIRECT_FLAG}) \
  $(echo ${SET_REDIRECT_URL}) \
  $(echo ${SET_JS_HOSTS}) \
	-v ~/.mitmproxy:/root/.mitmproxy/ \
	-v $(pwd)/assets/:/assets \
	redirect-proxy
