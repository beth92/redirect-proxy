#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Composes hooks for mitmproxy's mitmdump:
  - intercepts requests routed through localhost:8080
  - serves static files from /assets/ directory at test.proxyapp.com
  - then routes all requests to <somewebsite.com>/assets/* to test.proxyapp.com
  - if redirect option is set, redirects JS library requests to specified redirect URL
"""

from mitmproxy import http
from bottle import Bottle, static_file
from mitmproxy.addons import wsgiapp
from mitmproxy.net.http.headers import Headers
from os.path import expanduser
from typing import Tuple, List
import os


def get_redirect_options() -> Tuple[bool, str, List[str]]:
    url_arg = os.environ.get('REDIRECT_URL', None)
    should_redirect = os.environ.get('REDIRECT_JS') == 'true' or url_arg is not None
    redirect_url = 'http://localhost:4200' if should_redirect and url_arg is None else url_arg
    js_lib_hosts = os.environ.get('JS_LIB_HOSTS', 'js-cdn.example.com,js.example.com').split(',')
    return should_redirect, redirect_url, js_lib_hosts


def is_lib_request(flow: http.HTTPFlow) -> bool:
    # allow blacklisting of lib requests to <js lib host>/exclusions/lib.js
    never_redir = ['exclusions']
    path = flow.request.path_components
    return any(flow.request.pretty_host.endswith(x) for x in JS_LIB_HOSTS) and (len(path) == 0 or path[0] not in never_redir)


def is_test_page(flow: http.HTTPFlow) -> bool:
    path = flow.request.path_components
    return len(path) >= 2 and path[0] == "assets"


def redirect_js_request(flow: http.HTTPFlow) -> None:
    # Sends a regular redirect, so the browser doesn't use the proxy
    location = REDIRECT_URL + flow.request.path
    resp = http.HTTPResponse(
       "HTTP/1.1",
       302,
       "Temporary Redirect",
       Headers(
           Content_Type="text/html",
           Location=location,
           access_control_allow_origin="*"
       ),
       b'<html>Redirect</html>'
    )
    flow.response = resp


def redirect_flow_to_test_domain(flow: http.HTTPFlow) -> None:
    flow.request.host = "test.proxyapp.com"


def request(flow: http.HTTPFlow) -> None:
    if SHOULD_REDIRECT_JS and is_lib_request(flow):
        redirect_js_request(flow)
    elif is_test_page(flow):
        redirect_flow_to_test_domain(flow)


(SHOULD_REDIRECT_JS, REDIRECT_URL, JS_LIB_HOSTS) = get_redirect_options()
print('\033[92m**Running proxy-redirect app**\033[0m'.format(REDIRECT_URL))
if SHOULD_REDIRECT_JS:
    print(f'\033[94m**Redirecting JS library requests to {REDIRECT_URL}**\033[0m')
    print('\033[94m**For JS hosted at the following domains:**\033[0m')
    [print(f'\033[94m - {host}\033[0m') for host in JS_LIB_HOSTS]


app = Bottle()


@app.route('/')
def test_page_root() -> str:
    return 'Serving files from /assets directory'


@app.route('/assets/<filename>')
def serve_static(filename):
    return static_file(filename, root="/assets")


def serve_cert(ext):
    return static_file('.mitmproxy/mitmproxy-ca-cert.' + ext,
                       root=expanduser("~"), mimetype='application/pkcs-12', download=True)


@app.route('/assets/ca-cert.p12')
def serve_cert_p12():
    return serve_cert("p12")


@app.route('/assets/ca-cert.pem')
def serve_cert_pem():
    return serve_cert("pem")


addons = [
    wsgiapp.WSGIApp(app, "test.proxyapp.com", 80),
    wsgiapp.WSGIApp(app, "test.proxyapp.com", 443)
]
