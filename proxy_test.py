import proxy
from mitmproxy.test import tflow, tutils


class TestProxy:
    def test_is_lib_request(self):
        lib_reqs = [
            tflow.tflow(req=tutils.treq(
                host="js-cdn.example.com",
                path="/a/library.com.js"
            )),
            tflow.tflow(req=tutils.treq(
                host="js.example.com",
                path="/a/helper.js"
            ))
        ]
        non_lib_reqs = [
            tflow.tflow(req=tutils.treq(
                host="analytics.sortable.com",
                path="/command-centre/home"
            )),
            tflow.tflow(req=tutils.treq(
                host="js.example.com",
                path="/exclusions/lib.min.js"
            ))
        ]
        for req in lib_reqs:
            assert proxy.is_lib_request(req)
        for req in non_lib_reqs:
            assert proxy.is_lib_request(req) is False

    def test_is_test_page(self):
        test_page_reqs = [
            tflow.tflow(req=tutils.treq(
                host="example.com",
                path="/assets/hello-world.html"
            )),
            tflow.tflow(req=tutils.treq(
                host="staging.website.com",
                path="/assets/test.css"
            )),
        ]
        regular_reqs = [
            tflow.tflow(req=tutils.treq(
                host="analytics.example.com",
                path="/dashboard/home"
            )),
            tflow.tflow(req=tutils.treq(
                host="mysite.com",
                path="/assets"
            )),
        ]
        for req in test_page_reqs:
            assert proxy.is_test_page(req)
        for req in regular_reqs:
            assert proxy.is_test_page(req) is False

    def test_redirect_js_lib(self):
        lib_req = tflow.tflow(req=tutils.treq(
            host="js-cdn.example.com",
            path="/a/library.min.js"
        ), resp=True)
        proxy.redirect_js_request(lib_req)
        resp = lib_req.response
        assert resp.status_code == 302
        assert resp.headers['location'] == 'http://localhost:4200/a/library.min.js'

    def test_redirect_flow_to_test_domain(self):
        asset_path = "/assets/test-styles.css"
        flow = tflow.tflow(req=tutils.treq(
            host="example.com",
            path=asset_path
        ))
        proxy.request(flow)
        assert flow.request.pretty_host == "test.proxyapp.com"
        assert flow.request.path == asset_path
